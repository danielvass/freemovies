package com.prueba.freemovies.mapper;


import com.prueba.freemovies.dto.MovieDto;
import com.prueba.freemovies.entity.Movie;
import org.mapstruct.*;


@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MovieMapper {


    MovieDto movieToMovieDto(Movie movie);
@InheritInverseConfiguration
    Movie movieDtoToMovie(MovieDto movieDto);
}
