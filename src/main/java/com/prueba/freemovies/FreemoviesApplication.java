package com.prueba.freemovies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FreemoviesApplication {

	public static void main(String[] args) {
		SpringApplication.run(FreemoviesApplication.class, args);
	}

}
