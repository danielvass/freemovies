package com.prueba.freemovies.service.impl;
import com.prueba.freemovies.dto.MovieDto;
import com.prueba.freemovies.entity.Movie;
import com.prueba.freemovies.exception.CustomException;
import com.prueba.freemovies.mapper.MovieMapper;
import com.prueba.freemovies.repository.MovieRepository;
import com.prueba.freemovies.service.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import java.util.stream.Collectors;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private MovieMapper movieMapper;
    private static final Logger logger = LoggerFactory.getLogger(MovieService.class);
    @Override
    public List<MovieDto> getAllMovies() {
        try {
            List<Movie> movies = movieRepository.findAll();
            return movies.stream().map(movie -> movieMapper.movieToMovieDto(movie)).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Error retrieving movies", e);
            throw new CustomException("Error retrieving movies", 0);
        }
    }

    @Override
    public MovieDto getMovieById(Long id) {
        return movieRepository.findById(id)
                .map(movieMapper::movieToMovieDto)
                .orElse(null);
    }

    @Override
    public MovieDto createMovie(MovieDto movieDto) {
        Movie movie = movieMapper.movieDtoToMovie(movieDto);
        movie = movieRepository.save(movie);
        return movieMapper.movieToMovieDto(movie);
    }

    @Override
    public MovieDto updateMovie(Long id, MovieDto movieDto) {
        return movieRepository.findById(id)
                .map(existingMovie -> {
                    Movie movie = movieMapper.movieDtoToMovie(movieDto);
                    movie.setId(existingMovie.getId()); // Preservar el ID existente
                    movie = movieRepository.save(movie);
                    return movieMapper.movieToMovieDto(movie);
                })
                .orElse(null);
    }

    @Override
    public boolean deleteMovie(Long id) {
        if (movieRepository.existsById(id)) {
            movieRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}