package com.prueba.freemovies.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor

public class AuthResponse {
    String token;
    public AuthResponse(String token) {
        this.token = token;
    }
}